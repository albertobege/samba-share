# Samba Share

This is only a tutorial for how to mount a samba shared folder in a local folder and making it automatic remount at system boot up.

This method is running on ubuntu 20 LTS.

1. install fsutils
```
sudo apt-get install cifs-utils
```

2. create mount point
```
sudo mkdir /mnt/mountpoint_path
```

2. edit /etc/fstab file adding this row
```
//server_ip/share_path /mnt/mountpoint_path cifs credentials=/home/username/.smbcredentials,uid=shareuser 0 0
```

3. create .smbcredentials file
```
username=shareuser
password=sharepassword
domain=domain_or_workgroupname
```

4. secure .smbcredentials file
```
chmod 0600 ~/.smbcredentials
```

5. test mount
```
sudo mount -a
```

6. enjoy
